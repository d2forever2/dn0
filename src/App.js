import React from 'react';
import {
  BrowserRouter as Router,
  Route,
  Switch
} from 'react-router-dom';

import StaticPage from './StaticPage';
import Login from './components/login';

const App = () => {
  return (
    <Router>
      <Switch>
        <Route exact path="/" component={StaticPage} />
        <Route exact path="/login" component={Login} />
      </Switch>
    </Router>
  );
};

export default App;
