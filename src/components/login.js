import React, { useState } from 'react';
import "antd/dist/antd.css";
import { Input, Space, Table, Tag, Image } from 'antd';
import axios from 'axios';
import _ from 'lodash';
const { Search } = Input;

const StaticPage = () => {
  const [data, setData] = useState([]);
  const fixed = "fixed";
  const columns = [
    {
      title: 'Name',
      dataIndex: 'name',
    },
    {
      title: 'Tags',
      dataIndex: 'tags',
      render: tags => (
        <>
          {tags.map((tag, index) => {
            let color = tag.title.length > 6 ? 'geekblue' : 'green';

            if (tag.title.length < 6) {
              color = 'volcano';
            }
            return (
              <Tag color={color} key={`tag-${index + 1}`}>
                {tag.title.toUpperCase()}
              </Tag>
            );
          })}
        </>
      ),
    },
    {
      title: 'Image',
      dataIndex: 'image',
      render: (text, record) => {
        const image = record.image;
        return (
          <Image
            width={200} height={135}
            src={image}
          />
        );
      },
    },
  ];
  const debounce = (func, delay) => {
    let debounceTimer;
    return function () {
      const context = this;
      const args = arguments;
      clearTimeout(debounceTimer);
      debounceTimer =
        setTimeout(() => func.apply(context, args), delay);
    }
  }

  let handleValueChange1 = debounce(handleValueChange, 500);

  async function handleValueChange(e) {
    console.log("abc: ", e.target.value);
    const res = await axios.get(`https://api.unsplash.com/search/photos?client_id=RsZE_jBeDDbVS5iJChdWsxc-LrA6drTswAE6ufRPuXw&query=${e.target.value}`);
    let data = res.data.results;
    data = data.map((item, index) => {
      const columnData = item.user;
      const firstName = columnData.first_name;
      const lastname = columnData.last_name;
      const tag1 = item.tags;
      const images = item.urls.full;
      return {
        name: `${firstName} ${lastname}`,
        key: index + 1,
        tags: tag1,
        image: images,
      }
    });
    console.log(res.data.results);
    setData(data);
  }

  return (
    <div className="App">
      <Space direction="vertical">
        <Search placeholder="input search text" onChange={handleValueChange1} />
      </Space>
      <Table height="100px" columns={columns} dataSource={data} rowKey="key" pagination={{
        position: "absolute",
        showSizeChanger: true,
        pageSizeOptions: ["5", "10", "20", "30", "50"]
      }} />
    </div>
  );
}

export default StaticPage;
